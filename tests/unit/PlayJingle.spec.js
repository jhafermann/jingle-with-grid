import { expect } from 'chai'
import { shallowMount } from '@vue/test-utils'
import PlayJingle from '@/components/PlayJingle.vue'
import i18n from '@/i18n.js'
import _ from 'lodash';



describe('PlayJingle.vue', () => {
  it('h3 tag with localized id rendered', () => {
    i18n.locale = "de"
    const item = {"id": "random"};
    const wrapper = shallowMount(PlayJingle, {
      propsData: { 
        item: item 
      },
      i18n
    })
    const h3 = wrapper.find('h3')        
    expect(h3.text()).to.be.equal('zufällig')
  })

  it('prop item assigned', () => {
    const item = {"id": "random"};
    const wrapper = shallowMount(PlayJingle, {
      propsData: {
        item: item
      },
      i18n
    })
    expect(wrapper.props().item).to.deep.equal({"id": "random"})
  })

  it('play sound function is emitting event', () => {
    window.HTMLMediaElement.prototype.play = () => { /* do nothing */ };
    _.random = () => { return 1 };

    const item = {"id": "random"};
    const items = [
      {"x":0,"y":0,"w":2,"h":3,"i":"0","id":"random","static": true},
      {"x":2,"y":0,"w":2,"h":3,"i":"1","id":"fail"},
      {"x":4,"y":0,"w":2,"h":3,"i":"2","id":"suspense"}
    ];          
    const wrapper = shallowMount(PlayJingle, {
      propsData: {
        item: item,
        items: items
      },
      i18n
    })
    wrapper.find('.button').trigger('click');
    expect(wrapper.emitted()).to.have.property('audio-playing')
    let result = {
      "audio-playing": [
        [
          {"id": "random"}
        ]
      ]
    }
    expect(wrapper.emitted()).to.deep.equal(result)
  })


})
