import Vue from 'vue'
import Vuex from 'vuex'

Vue.use(Vuex)

const defaultLayout = () => {
  return [
    {"x":0,"y":0,"w":2,"h":3,"i":"0","id":"random","static": true},
    {"x":2,"y":0,"w":2,"h":3,"i":"1","id":"fail"},
    {"x":4,"y":0,"w":2,"h":3,"i":"2","id":"suspense"},
    {"x":6,"y":0,"w":2,"h":3,"i":"3","id":"applause"},
    {"x":8,"y":0,"w":2,"h":3,"i":"4","id":"bigben"},
    {"x":10,"y":0,"w":2,"h":3,"i":"5","id":"bullshit"},
    {"x":0,"y":3,"w":2,"h":3,"i":"6","id":"imperial"},
    {"x":2,"y":3,"w":2,"h":3,"i":"7","id":"jeopardy"},
    {"x":4,"y":3,"w":2,"h":3,"i":"8","id":"angie"},
    {"x":6,"y":3,"w":2,"h":3,"i":"9","id":"dramatic"},
    {"x":8,"y":3,"w":2,"h":3,"i":"10","id":"mario"},
    {"x":10,"y":3,"w":2,"h":3,"i":"11","id":"barack"},
    {"x":0,"y":6,"w":2,"h":3,"i":"12","id":"crying"},
    {"x":2,"y":6,"w":2,"h":3,"i":"13","id":"horns"},
    {"x":4,"y":6,"w":2,"h":3,"i":"14","id":"illuminati"},
    {"x":6,"y":6,"w":2,"h":3,"i":"15","id":"shutup"},
    {"x":8,"y":6,"w":2,"h":3,"i":"16","id":"tadaah"},
    {"x":10,"y":6,"w":2,"h":3,"i":"17","id":"bazinga"},
    {"x":0,"y":9,"w":2,"h":3,"i":"18","id":"yeah"},
    {"x":2,"y":9,"w":2,"h":3,"i":"19","id":"hallelujah"},
    {"x":4,"y":9,"w":2,"h":3,"i":"20","id":"unicorn"}
  ]
}

export default new Vuex.Store({
  state: {
    playlist: [],
    layout: defaultLayout()
  },
  getters: {
    lastJingle: state => {
      return state.playlist[state.playlist.length-1];
    }
  },
  mutations: {
    addJingleToPlaylist (state, payload) {
      state.playlist.push(payload)
    },
    clearPlaylist (state){
      state.playlist = [];
    },
    updateLayout (state, payload){
      state.layout = payload;
    },
    resetLayout (state){
      state.layout = defaultLayout();
    }
  },
  actions: {
  }
})
