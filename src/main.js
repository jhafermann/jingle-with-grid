import Vue from 'vue'
import App from './App.vue'
import loadable from '@abas/elements/src/directives/loadable';
import i18n from './i18n'
import store from './store'

Vue.directive('loadable', loadable);

Vue.config.productionTip = false

i18n.locale = navigator.language;

new Vue({
  i18n,
  store,
  render: h => h(App)
}).$mount('#app')
